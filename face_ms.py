import json
import requests
from bs4 import BeautifulSoup
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

subscription_key = "f5ca4a90f2ad4df3b48c10d9397f5229"
assert subscription_key

face_api_url = 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect'
headers = {'Ocp-Apim-Subscription-Key': subscription_key}
params = {
    'returnFaceId': 'true',
    'returnFaceLandmarks': 'false',
    'returnFaceAttributes': 'emotion',
}

# MS Azure Open Vision API
def face_process(url):
    response = requests.post(face_api_url, params=params, headers=headers, json={"url": url})
    if response.status_code == 400 :
        return -1
    else :
        return json.dumps(response.json())

#-------------------------------------------------------------------------------------------------------

#시간 줄이기 위해 계산된 데이터 적용
exciting_weight = [0.25, 0.125, 0.0625, 0.0625, 1.0, 0.0625, 0.0625, 0.0625]
ssuckso_weight = [0.060591371788657304, 1.030053320407174, 0.060591371788657304, 0.060591371788657304, 0.42413960252060107, 0.24236548715462922, 0.060591371788657304, 0.060591371788657304]
serious_weight = [0.07142857142857144, 0.07142857142857144, 0.07142857142857144, 0.07142857142857144, 0.07142857142857144, 1.0, 1.0, 0.07142857142857144]

# 초기 데이터 기반 weight 설정(시작시 동시에 실행, 한번만)
def init() :
	# 모드별 대표 프로토타입 이미지
	#exciting_url = ["https://i.pinimg.com/originals/f9/f2/27/f9f2276f4bcc8993352fafcb4d9abb4a.jpg","https://scontent-lga3-1.cdninstagram.com/vp/56a472c2970f68371c00c39e51372e86/5D866659/t51.2885-15/e35/p1080x1080/62146139_528870227645299_9079180024449663749_n.jpg?_nc_ht=scontent-lga3-1.cdninstagram.com","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQji75zylq_fOh4VwbdH11uV5v7DkDaorCtd3Yd3WWSRu7KB90m","http://www.jkjtkd.kr/album/ed/1264836987.jpg","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqYC7dk-KwTveKxEeOzM7ObZbi1xmttQmdh5IOeuZoGCkpHnJj7A","https://dispatch.cdnser.be/wp-content/uploads/2018/08/20180821183723_shj_5713.jpg"]
	#ssuckso_url = ["https://pbs.twimg.com/media/CBgZjb_WEAAR54r.jpg","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEHQ8Wa3YLOdYJnlPPU7AWg8AKFJZzV5QzSCdNjPUCQhfMHIGN","https://img1.daumcdn.net/thumb/R720x0.q80/?scode=mtistory2&fname=http%3A%2F%2Fcfile25.uf.tistory.com%2Fimage%2F1138A01B4CEBAF544EF623","https://image.fmkorea.com/files/attach/new/20180623/3655109/674687851/1119180529/a8abf2dec880ee1f99b9614787e77f90.jpg","http://pds7.cafe.daum.net/download.php?grpid=16768&fldid=5neg&dataid=2&fileid=1&disk=17&.jpg"]
	#serious_url = ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ78khkB1th0Uey2KQG6FOR5hNNngOeXzdW9Iooe7m8CKXkaJTqCQ","http://image.edaily.co.kr/images/Photo/files/NP/S/2016/02/PS16020200584.jpg","http://pds.skyedaily.com/news_data/20190417123150_ffumgfxi.jpg","http://ph.spotvnews.co.kr/news/photo/201904/279991_344861_4914.jpg","http://dn.joongdo.co.kr/mnt/images/file/2018y/10m/16d/2018101601001528700065951.jpg","http://thumb.zumst.com/530x0/http://static.news.zumst.com/images/9/2016/02/02/NISI20160202_0011310361_web.jpg","https://images.freeimages.com/images/premium/previews/1264/12646731-man-with-serious-facial-expression.jpg"]
	#시간 줄이기 위해 계산된 데이터 적용
	# exciting_weight = [0.25, 0.125, 0.0625, 0.0625, 1.0, 0.0625, 0.0625, 0.0625]
	# ssuckso_weight = [0.060591371788657304, 1.030053320407174, 0.060591371788657304, 0.060591371788657304, 0.42413960252060107, 0.24236548715462922, 0.060591371788657304, 0.060591371788657304]
	# serious_weight = [0.07142857142857144, 0.07142857142857144, 0.07142857142857144, 0.07142857142857144, 0.07142857142857144, 1.0, 1.0, 0.07142857142857144]
	
	#weight설정
	'''
	exciting_value = []
	ssuckso_value = []
	serious_value = []
	# 모드 별 초기 가중치 설정
	exciting_weight = [0.2,0.1,0.05,0.05,0.8,0.05,0.05,0.05]
	ssuckso_weight = [0.05,0.85,0.05,0.05,0.35,0.2,0.05,0.05]
	serious_weight = [0.05,0.05,0.05,0.05,0.05,0.7,0.7,0.05]
	exciting_max = 0.0
	ssuckso_max = 0.0
	serious_max = 0.0
	# emotion_data = {}
	# 지표 8개에 대하여 3개모드 값 정규화 비율 계산
	for i in exciting_url :
		ok = 0
		face_data = face_recog(i) #json 데이터를 string으로 결과 가져옴
		if(face_data != -1) :
			while(ok!=1):
				emotion_data = json.loads(face_data) #string으로 된 데이터를 json.loads()하면 리스트로 변환
				try:
					emotion_data = emotion_data[0] #리스트의 0번째 인덱스가 받은 정보(dict)
					ok = 1
					emotion_data = emotion_data["faceAttributes"]["emotion"] #emotion 정보 또한 dict
					index = 0
					value = 0.0
					for key in emotion_data.keys():
						value += emotion_data[key] * 100 * exciting_weight[index] #감정에 대한 값은 float
						index += 1
					if(exciting_max < value) : exciting_max = value
					exciting_value.append(value)
				except KeyError:
					face_data = face_recog(i) #json 데이터를 string으로 결과 가져옴
	for i in ssuckso_url :
		face_data = face_recog(i) #json 데이터를 string으로 결과 가져옴
		ok = 0
		if(face_data != -1) :
			while(ok!=1):
				emotion_data = json.loads(face_data) #string으로 된 데이터를 json.loads()하면 리스트로 변환
				try:
					emotion_data = emotion_data[0] #리스트의 0번째 인덱스가 받은 정보(dict)
					ok = 1
					emotion_data = emotion_data["faceAttributes"]["emotion"] #emotion 정보 또한 dict
					index = 0
					value = 0.0
					for key in emotion_data.keys():
						value += emotion_data[key] * 100 * ssuckso_weight[index] #감정에 대한 값은 float
						index += 1
					if(ssuckso_max < value) : ssuckso_max = value
					ssuckso_value.append(value)
				except KeyError:
					face_data = face_recog(i)
	for i in serious_url :
		face_data = face_recog(i) #json 데이터를 string으로 결과 가져옴
		ok = 0
		if(face_data!=-1) :
			while(ok!=1):
				emotion_data = json.loads(face_data) #string으로 된 데이터를 json.loads()하면 리스트로 변환
				try:
					emotion_data = emotion_data[0] #리스트의 0번째 인덱스가 받은 정보(dict)
					ok = 1
					emotion_data = emotion_data["faceAttributes"]["emotion"] #emotion 정보 또한 dict
					index = 0
					value = 0.0
					for key in emotion_data.keys():
						value += emotion_data[key] * 100 * serious_weight[index] #감정에 대한 값은 float
						index += 1
					if(serious_max < value) : serious_max = value
					serious_value.append(value)
				except KeyError:
					face_data = face_recog(i)
	exciting_rate = 100.0 / exciting_max
	ssuckso_rate = 100.0 / ssuckso_max
	serious_rate = 100.0 / serious_max
	
	# 모드 3개 가중치 Normalization
	for i in range(0,len(exciting_weight)) :
		exciting_weight[i] = exciting_weight[i] * exciting_rate
	for i in range(0,len(ssuckso_weight)) :
		ssuckso_weight[i] = ssuckso_weight[i] * ssuckso_rate
	for i in range(0,len(serious_weight)) :
		serious_weight[i] = serious_weight[i] * serious_rate
	'''
	return exciting_weight, ssuckso_weight, serious_weight

# 사용자 지표 검출
def user_set(url, face_data):
	tmpvalue = []
	#face_data = face_process(url) #json 데이터를 string으로 결과 가져옴
	ok = 0
	if(face_data!=-1) :
		while(ok!=1):
			emotion_data = json.loads(face_data) #string으로 된 데이터를 json.loads()하면 리스트로 변환
			try:
				emotion_data = emotion_data[0] #리스트의 0번째 인덱스가 받은 정보(dict)
				ok = 1
				emotion_data = emotion_data["faceAttributes"]["emotion"] #emotion 정보 또한 dict
				for key in emotion_data.keys():
					tmpvalue.append(emotion_data[key] * 100) #감정에 대한 값은 float
			except KeyError:
				face_data = face_process(url)
		return tmpvalue
	else : return -1

# 사용자 점수 계산(API를 통해 새로 만든 2,3,4 모드는 각 가중치 곱해서)
def score(mode,value,exciting,ssuckso,serious):
	answer = 0.0
	#화남
	if(mode=='화남'):
		 print("AngryMode")
		 return value[0]
	#신나는 모드
	elif(mode=='신나는'):
		 print("ExcitingMode")
		 for i in range(0,len(value)):
			 answer += (value[i] * exciting[i])
		 return answer
	#썩은미소 모드
	elif(mode=='썩소'):
		print("SsuckSoMode")
		for i in range(0,len(value)):
			answer += (value[i] * ssuckso[i])
		return answer
	#심각한 모드
	elif(mode=='심각한'):
		print("SeriousMode")
		for i in range(0,len(value)):
			answer += (value[i] * serious[i])
		return answer
	#놀람 모드
	elif(mode=='놀람'):
		print("SurpriseMode")
		return value[7]


# def example_crawl():
# 	ex_url = "https://search.daum.net/search?w=img&nil_search=btn&DA=NTB&enc=utf8&q=%ED%99%94%EB%82%9C%EC%82%AC%EB%9E%8C%ED%91%9C%EC%A0%95"
# 	sourcecode = urllib.request.urlopen(ex_url).read()
# 	soup = BeautifulSoup(sourcecode, "html.parser")
# 	cnt = 5
# 	angry_example=[]
# 	title = soup.find_all("div", class_="wrap_thumb")
# 	print(title)
# 	for i in range(3,len(title)):
# 		if(cnt==0) : break
# 		link = title[i].find("img")["data-src"]
# 		print(link)
# 		ex_value = user_set(link)
# 		if(ex_value!=-1):
# 			tmp_value = score(1,ex_value,0.0,0.0,0.0)
# 			if(tmp_value >= 90):
# 				angry_example.append(link)
# 				cnt = cnt - 1
# 	print(angry_example)
# def initialization():
# 	# 메인 시작
#     exciting_final, ssuckso_final, serious_final = init() # 초기 대표 사진들 통해 가중치 설정

def emotionCalculator(user_url, mode, face_data):
    # 유저사진 input
    #user_url = "https://image.fmkorea.com/files/attach/new/20180623/3655109/674687851/1119180529/a8abf2dec880ee1f99b9614787e77f90.jpg"
    # 유저사진 OPEN API 지표 분석
	user_value = user_set(user_url, face_data)
    # 유저사진 지표 모드에 맞게 계산
	user_score = score(mode, user_value, exciting_weight, ssuckso_weight, serious_weight)	
	round(user_score,2)
	print("Your Score : " + str(user_score))
	return user_score #float
	# 화남 / 신나는 / 썩소 / 심각한 / 놀람
	#  1      2       3      4        5