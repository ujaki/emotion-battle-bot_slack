# -*- coding: utf-8 -*-
import threading
import re
import os
import json
import requests
import urllib.request
import urllib.parse
from face_ms import face_process, emotionCalculator #Face API
from bs4 import BeautifulSoup
from flask import Flask, request, jsonify
from slack import WebClient
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter


OAUTH_TOKEN = "xoxp-682997044292-675618248498-689164281028-cc052e3d3c9eefab8d4597f705ed4dfb"
SLACK_TOKEN = "xoxb-682997044292-691779399126-ndwUpTWftQeJhnGAKUwuxTMY"
SLACK_SIGNING_SECRET = "259cd8102d873d6c76fb0486785397e7"


valid_imag_file_format_list = ['jpg', 'gif', 'bmp', 'png']

app = Flask(__name__)
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

#-------------------------------------------------------
accessed_channels = []  #봇에 접속한 여러 채널 정보 모음
user_data = {}          #유저별 요청에 따른 응답 대응하기 위해 --> 추후 DB로 관리 또는 class 형태로 변환
#-------------------------------------------------------

#챗봇에게 아무 응답도 하지 않은 사용자들에게 놀자고 메시지 뿌리기
#더 이상 나를 찾지 말라고 한 사용자들에게는 메시지 뿌리지 않기 user_data에 사용자 id가 없거나 / state = 0인 사람들
timer_duration = 60 #in seconds
def letsHaveFun():
    sendMsgHaveFun() #주기적으로 반복
    threading.Timer(timer_duration, letsHaveFun).start() #타이머 시작

def sendMsgHaveFun():
    print("쓰레드드드드드ㅡ")
    if len(user_data) == 0: return #힝.... 아무도 나랑 놀 사람이 없네
    else:
        response = slack_web_client.users_list()
        for user in response["members"]:
            if user["id"] in user_data:
                #print(user["id"])
                if user_data[user["id"]]["state"] == 0: #게임 시작 전 상태
                    norazo_url = "https://media1.tenor.com/images/f13d5c8fd60b46fd2177c8b028742aba/tenor.gif?itemid=13153991"
                    block = ImageBlock(image_url = norazo_url, alt_text = "너랑 게임할 기분이 아니야 ㅠㅠㅠㅠ")
                    #채널 정보를 바꿀 수 있는지???
                    slack_web_client.chat_postEphemeral(channel = "#3-1", user = user["id"], blocks = extract_json([block]))
                    slack_web_client.chat_postEphemeral(channel = "#3-1", user = user["id"], text = "\n노라조 노라조 나랑 놀아줘~~~")
                elif user_data[user["id"]]["state"] == -1: #게임 거부 해놓은 상태
                    slack_web_client.chat_postEphemeral(channel = "#3-1", user = user["id"], text = "\n힝 너무해..... 너 내가 싫어?")
#업로드한 이미지 파일의 URL public 권한을 true로 변경하기 위한 API
#URL format
#https://slack.com/api/files.sharedPublicURL?token=xoxp-" + OAUTH_TOKEN + "&file=" + "file id" + "&pretty=1"
# file_request_header = {'application/x-www-form-urlencoded': "application/json"}
# file_shared_url = "https://slack.com/api/files.sharedPublicURL"






'''
user_data = [{사용자 1 : {state : 0, grade : 0, emotion = ''},
         {사용자 2 : {state : 0, grade : 0, emotion = ''},
         {사용자 1 : {state : 0, grade : 0, emotion = ''}]
'''  

def showRestartGame():
    head_section = SectionBlock(
            text="*그래도 한번은 봐줄게 어떄? 게임 할래? (^_^)*\n"
        )
    button_actions = ActionsBlock(
        block_id = "restart_game",
        elements=[
            #어떠한 감정 모드를 선택 하겠는가?
            ButtonElement(
                text="Yes",
                action_id="restart_Yes", value='restart_Yes'  
            ),
            ButtonElement(
                text="No", style="danger",
                action_id="restart_No", value='restart_No' 
            )
        ]
    )
    foot_section = SectionBlock(
        text="둘 중에 하나만 골라 Yes or Yes~~~"
    )
    # 각 섹션을 list로 묶어 전달합니다
    return [head_section, button_actions,foot_section]

def resBuilderForES():
    head_section = SectionBlock(
            text="*빨리 빨리~~~*\n"
        )
    button_actions = ActionsBlock(
        block_id = "emotion_select",
        elements=[
            #어떠한 감정 모드를 선택 하겠는가?
            ButtonElement(
                text="화남",
                action_id="emotion1", value='화남' # annoyed
            ),
            ButtonElement(
                text="신나는", style="danger",
                action_id="emotion2", value='신나는' #신나는 funny
            ),
            ButtonElement(
                text="썩은미소",style="primary",
                action_id="emotion3", value='썩소' #ssuckso
            ),
            ButtonElement(
                text="심각한", 
                action_id="emotion4", value='심각한' # serious
            ),ButtonElement(
                text="놀람", style="danger",
                action_id="emotion5", value='놀람' # tired
            ),
        ]
    )
    foot_section = SectionBlock(
        text="게임을 종료하시려면 '@EBB 종료' 라고 입력 해주세요"
    )
    # 각 섹션을 list로 묶어 전달합니다
    return [head_section, button_actions,foot_section]


def resBuilderForRank():
    head_section = SectionBlock(
            text="Are you ready~~~?!!!!")
    button_actions = ActionsBlock(
        #버튼을 정렬하는 옵션을 넣으면 좋을 것 같음
        block_id = "start_button",
        elements=[
            ButtonElement(
                text="시작",
                action_id="start_button", value='start_button'
            )
        ]
    )
    foot_section = SectionBlock(
        text="게임을 종료하시려면 '@EBB 종료' 라고 입력 해주세요"
    )
    return [head_section, button_actions,foot_section]

def resBuilderForChallange():
    head_section = SectionBlock(
            text="빨리 빨리~~~")
    button_actions = ActionsBlock(
        #버튼을 정렬하는 옵션을 넣으면 좋을 것 같음
        block_id = "score_selection",
        elements=[
            ButtonElement(
                text="70점 이상",
                action_id="70", value='70'
            ),
            ButtonElement(
                text="90점 이상", style="danger",
                action_id="90", value='90'
            )
        ]
    )
    foot_section = SectionBlock(
        text="게임을 종료하시려면 '@EBB 종료' 라고 입력 해주세요"
    )
    return [head_section, button_actions,foot_section]

def resBuilderForGameReady():
    head_section = SectionBlock(
            text="빨리 빨리~~~")
    button_actions = ActionsBlock(
        #버튼을 정렬하는 옵션을 넣으면 좋을 것 같음
        block_id = "mode_selection",
        elements=[
            ButtonElement(
                text="도전 모드",
                action_id="mode_challenge", value='mode_challenge'
            ),
            ButtonElement(
                text="랭크 모드", style="danger",
                action_id="mode_rank", value='mode_rank'
            )
        ]
    )
    foot_section = SectionBlock(
        text="게임을 종료하시려면 '@EBB 종료' 라고 입력 해주세요"
    )
    return [head_section, button_actions,foot_section]

def resBuilderForRandomMention():
    # 메시지를 꾸밉니다
    # 마지막 섹션에는 가격대를 바꾸는 버튼을 추가합니다
    # 여러 개의 버튼을 넣을 땐 ActionsBlock을 사용합니다 (버튼 5개까지 가능)
    head_section = SectionBlock(
            text="다음 3개의 옵션중 원하는 것을 선택하시오.")
    button_actions = ActionsBlock(
        #버튼을 정렬하는 옵션을 넣으면 좋을 것 같음
        block_id = "random_mention",
        elements=[
            ButtonElement(
                text="게임 시작",
                action_id="game_start_now", value='game_start_now'
            ),
            ButtonElement(
                text="나중에 할래", style="danger",
                action_id="game_start_later", value='game_start_later'
            ),
            ButtonElement(
                text="방해하지마!",style="primary",
                action_id="game_start_never", value='game_start_never'
            )
        ]
    )
    foot_section = SectionBlock(
        text="게임을 종료하시려면 '@EBB 종료' 라고 입력 해주세요"
    )
    return [head_section, button_actions,foot_section]
    
#Slack sharedPublicURL 주소 빌더
def publicURLbuilder(file_id):
    return "https://slack.com/api/files.sharedPublicURL?token=" + OAUTH_TOKEN + "&file=" + str(file_id) + "&pretty=1"

def parseEmotionData(face_data, channel_id, user_id):
    emotion_data = json.loads(face_data) #string으로 된 데이터를 json.loads()하면 리스트로 변환
    emotion_data = emotion_data[0]       #리스트의 0번째 인덱스가 받은 정보(dict)
    emotion_data = emotion_data["faceAttributes"]["emotion"] #emotion 정보 또한 dict
    data_list = []
    for emotion, value in emotion_data.items():
        temp_str = "{} : {}".format(emotion, str(value))
        data_list.append(temp_str)
    msg = '\n'.join(data_list)
    slack_web_client.chat_postEphemeral(channel = channel_id, user = user_id, text = "분석 결과는 다음과 같아~\n" + msg)

#사용자들에게 챗봇이 메세지 보내는 함수
def sendMsgToUsers(channel_id, user_id, message): 
    slack_web_client.chat_postEphemeral(channel = channel_id, user = user_id, text = "<@{}>\n".format(user_id) + message)

#사용자들에게 챗봇이 블럭을 보내는 함수
def sendBlockToUsers(channel, msg_blocks): 
    slack_web_client.chat_postMessage(channel=channel, blocks = extract_json(msg_blocks))

#구현중
def imageFileSave(file_url):
    #url = file['url_private']
    res = requests.get(file_url, headers={'Authorization': 'Bearer %s' % OAUTH_TOKEN})
    print(res)
    if res.status_code == 200:
        with open('ok.jpg', 'wb') as f:
            for data in res:
                f.write(data)

# 사용자가 버튼을 클릭한 결과는 /click 으로 받습니다 이 기능을 사용하려면 앱 설정 페이지의 "Interactive Components"에서 /click 이 포함된 링크를 입력해야 합니다.
@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    # 버튼 클릭은 SlackEventsApi에서 처리해주지 않으므로 직접 처리합니다
    payload = request.values["payload"]
    data = json.loads(payload)
    print(data)
    user_id = data["user"]["id"]
    channel_id = data["channel"]["id"]
    actions = data["actions"][0]
    block_id = actions["block_id"]
    action_id = actions["action_id"]
    value = actions["value"]
    #restart_Yes restart_No
    if value == "restart_Yes":
        user_data[user_id]["state"] = 0  #게임 대기로 변경
        slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "Welcome back!!!")
    elif value == "restart_No":
        user_data[user_id]["state"] = 0
        slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "실망이야 ㅠㅠㅠ")
    elif value == "game_start_never":
        user_data[user_id]["state"] = -1  #게임 안함
        user_data[user_id]["emotion"] = "무념 무상"
        slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "게임을 하기 싫다니...\n아쉽지만 다음에 또 만나요~")
    elif value == "game_start_later":
        user_data[user_id]["state"] = 0   #게임 시작 대기
        user_data[user_id]["emotion"] = "귀찮음"
        slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "언제까지 어깨춤을 추게할거야~\n내 관절을 봐~ 고장났잖아~")
    elif value == "game_start_now":
        user_data[user_id]["state"] = 1  #게임 준비
        user_data[user_id]["emotion"] = "기대됨"
        #slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "자~ 지금부터 게임을 시작하지!\n다음 게임 모드중 원하는 것을 선택해줘~~~")
        slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "자~ 지금부터 게임을 시작하지!\n먼저 원하는 감정 표현을 선택해줘~~~")
        slack_web_client.chat_postEphemeral(channel = channel_id, user = user_id, blocks = extract_json(resBuilderForES()))    
        
    elif value == 'mode_challenge':
        user_data[user_id]["state"] = 3  #도전 모드 선택
        #user_data[user_id]["emotion"] = "준비됨"
        slack_web_client.chat_postEphemeral(channel = channel_id, user = user_id, text = "도전 모드를 선택했네!!")
        slack_web_client.chat_postEphemeral(channel = channel_id, user = user_id, text = "준비 됐으면 다음 중 목표 점수를 선택해줘~~\n")
        slack_web_client.chat_postEphemeral(channel = channel_id, user = user_id, blocks = extract_json(resBuilderForChallange()))
    elif value == 'mode_rank':
        user_data[user_id]["state"] = 4  #랭크 모드 선택
        #user_data[user_id]["emotion"] = "준비됨"
        slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "랭크 모드를 선택했네!!")
        slack_web_client.chat_postEphemeral(channel = channel_id, user = user_id, text = "준비가 됐으면 시작 버튼을 눌러줘~~\n")
        slack_web_client.chat_postEphemeral(channel = channel_id, user = user_id, blocks = extract_json(resBuilderForRank()))
    elif value == "start_button":
        slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "게임이 시작됐습니다!!\n얼굴사진을 업로드 해주세요!!!")
    elif value == '화남' or value == '신나는' or value == '썩소' or value == '심각한' or value == '놀람': 
        slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "<{}> 감정 표현을 선택했네?\n자~ 이제 원하는 게임 모드를 선택해줘!!!".format(value))
        #게임 모드 선택 버튼들 보여주기
        slack_web_client.chat_postEphemeral(channel = channel_id, user = user_id, blocks = extract_json(resBuilderForGameReady()))
        user_data[user_id]["state"] = 2  #모드 선택 상태로 진입
        user_data[user_id]["emotion"] = value
        
    elif value == '70' or value == '90': 
        if user_data[user_id]["state"] == 3:
            slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "당신이 선택한 목표 점수는 {}점 입니다!!\n얼굴사진을 업로드 해주세요!!!".format(value))
            user_data[user_id]['grade'] = float(value)
            #user_data[user_id]["state"] = 4  #게임 중(도전 모드)
        else:
            slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "뭔가 잘못된 것 같은데...")
    else:
        slack_web_client.chat_postEphemeral(channel=channel_id, user = user_id, text = "잘못된 버튼 입력이야... 미안해 고치도록 할게!!!")
   
    #sendMsgToUsers(channel_id, user_id, "{} 모드를 선택하셨습니다.\n얼굴사진을 업로드 해주세요!!!".format(click_event.value))
    # Slack에게 클릭 이벤트를 확인했다고 알려줍니다
    return "OK", 200



#전 클라이언트 메세지의 id //재호출 방지
prev_client_msg_id = {}


# 챗봇이 멘션을 받으면 아래 코드 실행됨
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    #global user_data
    print(event_data)
    channel = event_data["event"]["channel"] #채널 정보 가져오기
    text = event_data["event"]["text"]       #텍스트 정보 가져오기
    user_id = event_data["event"]["user"]    #메시지를 보낸 사람의 ID
    client_msg_id = event_data["event"]["client_msg_id"]

    #새로운 채널에서 봇에 접속하면 채널 정보 등록
    if not channel in accessed_channels: 
        accessed_channels.append(channel)
    elif channel in accessed_channels:
        pass
    #text = "<@UDKDNS> text"
    #호출 재방지
    if client_msg_id not in prev_client_msg_id:
        prev_client_msg_id[client_msg_id] = 1
    else : 
        return
    
    #여기 조져야함
    #matches = re.search(r"<@U\w+>\s*시작\s*(\w+)", text)
    matches = re.search(r"<@U\w+>\s+(종료)", text)
    
    "시작 무슨 모드"
    # --> 버튼
    # --> 
    "종료"

    #이거 수정
    keyword_list = []
    # 반복을 의미하는 * 메타 문자가 사용되었다. 여기에서 사용한 *은 * 바로 앞에 있는 문자 a가 0부터 무한대로 반복될 수 있다는 의미
    # 반복을 나타내는 또 다른 메타 문자로 +가 있다. +는 최소 1번 이상 반복될 때 사용한다. 즉 *가 반복 횟수 0부터라면 +는 반복 횟수 1부터인 것

    #-------state------------------------
    # 0 : 게임 시작 전 상태
    # 1 : 게임 준비 상태
    # 2 : 도전 모드 준비
    # 3 : 랭크 모드 준비
    # 4 : 도전 모드중
    # 5 : 랭크 모드중
    # 6 : 이미지 분석
    # 5 : 이미지 분석이 끝난 상태 -> 2로 돌아옴
    # 7 : 종료중인 상태 -> 0으로 돌아옴

    #종료인지?
    #terminate = matches.group(1)
    #if terminate == "종료": print("termination")
    #print(matches.group(1))
    if not matches: #의도한 멘션을 받았을 경우가 아니라면
        if not user_id in user_data: #
            user_data[user_id] = {'state' : 0,'grade' : 0,'emotion':''}
            slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "호호호~~~ 어서와 나랑 게임하고 싶어??\n그렇다면 나에게 아무말이나 걸어줘~~")
        else:
            #현재 유저의 상태 정보를 보고 판단
            if user_data[user_id]["state"] == -1: #게임 안함
                slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "흥!칫!뿡!\n 게임 안 한다고 그랬잖아 다른 봇이랑 놀아!")
                slack_web_client.chat_postEphemeral(channel = channel, user = user_id, blocks = extract_json(showRestartGame()))
                #다시 기회줄 수 있는 부분?
            elif user_data[user_id]["state"] == 0: #게임 시작 전
                jigsqw_url ="https://media1.tenor.com/images/b7957d24a0fb1ddf98a4fb5876e1e76b/tenor.gif?itemid=4890666"  
                block = ImageBlock(image_url = jigsqw_url, alt_text = "너랑 게임할 기분이 아니야 ㅠㅠㅠㅠ")
                slack_web_client.chat_postEphemeral(channel = channel, user = user_id, blocks = extract_json([block]))
                #slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "\n너 나랑 게임할래??")
                #게임 시작, 나중에, 아니 버튼들 보여줌
                slack_web_client.chat_postEphemeral(channel = channel, user = user_id, blocks = extract_json(resBuilderForRandomMention()))
            elif user_data[user_id]["state"] == 1: #게임 준비 상태
                slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "빨리 원하는 감정 표현을 선택해줘~\n현기증 난단 말이야~~")
                #slack_web_client.chat_postEphemeral(channel = channel, user = user_id, blocks = extract_json(resBuilderForGameReady()))
            elif user_data[user_id]["state"] == 2: #모드 선택 상태 
                #게임 모드 선택 준비
                slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "빨리 원하는 게임 모드를 선택해줘~\n현기증 난단 말이야~~")
            elif user_data[user_id]["state"] == 3 or user_data[user_id]["state"] == 4: #도전 모드중
                #파일 업로드가 아니라면, 즉 텍스트만 챗봇이 받았을 때
                if not "files" in event_data["event"]:
                    if user_data[user_id]["state"] == 3:
                        slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "너의 사진을 기다리고 있어~\n현기증 난단 말이야~~")
                    elif user_data[user_id]["state"] == 4:  #랭크 모드 준비
                        slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "I wanna see your face~~~ 빨리 빨리!!!")
                    else:
                        slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "얼굴사진을 올려줘!!!! (^_^)")
                    #print("파일 업로드 안됨")
                    
                    # if user_data[user_id]['state'] == 4 or user_data[user_id]['state'] == 5:
                    #     slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "얼굴사진을 올려줘!!!! (^_^)")
                    # else:
                    #     slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "게임 모드 선택 후 얼굴 사진을 올려줘야지 (-_-*)")
                else:
                    file_info = event_data["event"]["files"][0] #업로드된 파일 정보 가져오기
                    file_name = file_info["url_private"].split('/')
                    file_name = file_name[-1]
                    file_format = str(file_info["filetype"])
                    if file_format in valid_imag_file_format_list: #유효한 이미지 파일을 받으면
                        #print("파일 분석 시작")
                        res = requests.post(publicURLbuilder(file_info["id"]))
                        if res.status_code == 200: #요청이 정상적으로 이루어지면 --> 이미지 분석 성공
                            #print("API 요청 성공")
                            parse_url = str(file_info["permalink_public"]).split('/')[-1] #TL2VB1A8L-FKZM42BFV-eabb82ec40
                            generated_code = '-'.join(parse_url.split('-')[:2])
                            pub_secret_code = parse_url.split('-')[-1]
                            image_url = "https://files.slack.com/files-pri/" + generated_code + "/" + file_name + "?pub_secret=" + pub_secret_code
                            #
                            face_data = face_process(image_url) #json 데이터를 string으로 결과 가져옴
                            if face_data == -1 : 
                                sendMsgToUsers(channel, user_id, "미안하지만 이 이미지는 분석할 수가 없네 (ㅠ_ㅠ)")
                            else:
                                #print(face_data)
                                #선택한 게임 모드에 따라서 처리
                                if user_data[user_id]["state"] == 3:  #도전 모드일 때
                                    #감정 표현 분석한 정보 print까지
                                    user_score = emotionCalculator(image_url, user_data[user_id]["emotion"], face_data)
                                    slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "당신의 점수는 {}점 입니다.\n".format(user_score))  
                                    if user_score < user_data[user_id]["grade"]:
                                        slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "좀 더 분발하세요!\n사진을 다시 찍어 올려보세요.")    
                                        #user_data[user_id]['state'] = 3
                                    else: #도전 성공!
                                        slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "도전에 성공하여 게임 종료")    
                                        user_data[user_id] = {'state' : 0,'grade' : 0,'emotion':''}
                                      
                                    parseEmotionData(face_data, channel, user_id)
                                    #게임 재시작
                                elif user_data[user_id]["state"] == 4: #랭크 모드일 때
                                    user_score = emotionCalculator(image_url, user_data[user_id]["emotion"], face_data)
                                    text_edit(user_score, user_id) #데이터 기록
                                    #랭크 보기 기능
                                    rank_view(channel, user_id)
                                    #parseEmotionData(face_data, channel, user_id)
                                    
                                    #게임 재시작(한번 하고 끝)
                                    slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "*게임을 종료합니다*")
                                    user_data[user_id] = {'state' : 0,'grade' : 0,'emotion':''}
                                else:
                                    pass
                                
                                #sendMsgToUsers(channel, user_id, "감정 표현 분석을 완료했어!!")
                            return "OK", 200
                        else: 
                            return
                    else:
                        sendMsgToUsers(channel, user_id, "미안하지만 분석할 수 없는 이미지 파일이야!!!\n다음과 같은 이미지 포멧만 가능해~~~\n" + ','.join(valid_imag_file_format_list))
            elif user_data[user_id]["state"] == 6:
                pass
            else:
                pass
    elif matches.group(1) == "종료":           #의도한 멘션을 받았을 경우
        if not user_id in user_data:
            #챗봇이 사용자에 대한 호감도 정보를 가지고 있고 이 데이터를 활용하면 더 진보된 챗봇을 만들 수 있음
            slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "게임 해보지도 않고 종료를 한다구?!!!")
            user_data[user_id] = {'state' : 0,'grade' : 0,'emotion':''}
        else:
            if user_data[user_id]["state"] >= 1 and user_data[user_id]["state"] <= 4:
                slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "*게임을 종료합니다*")
                user_data[user_id] = {'state' : 0,'grade' : 0,'emotion':''}
            else:
                slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "*게임 시작도 안 했는데 종료한다구? (ㅠ_ㅠ)*")
        #--------------------------------------------------------
    else:
        if not user_id in user_data:
            slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "*(문제 발생)*\n 봇에게 말을 걸어주세요")    
        else:
            user_data[user_id] = {'state' : 0,'grade' : 0,'emotion':''}
            slack_web_client.chat_postEphemeral(channel = channel, user = user_id, text = "*문제가 발생하여 게임을 종료합니다.*")
    
    prev_client_msg_id.pop(client_msg_id, None)
 
    return "OK", 200
 #------------------------------------------------------------  
'''
no_mentioned_list = []
no_mentioned_count = 
사용자에게 게임할래?를 물어보았을 떄 아니오를 카운트해서 
카운트 수가 높을 수록 봇이 더 짜증난듯(실망)한 문구를 보여줌
yes_mentioned_list = []
yes_mentioned_count = ?

'''





#랭크 모드일 때 (사진 하나만 올리게끔 제한)
def text_edit(score, user_id):
    response = slack_web_client.users_list()
    real_name = ''
    #user_id로 사용자의 이름 검색
    for data in response["members"]:
        if data['id'] == user_id :
            real_name = data['profile']['real_name']
            break

    #랭크 폴더 생성
    if not os.path.isdir("rank"):
        os.makedirs(os.path.join("rank"))
	#감정 모드에 대한 파일 생성
    if not os.path.isfile("rank/"+user_data[user_id]['emotion']+".txt" ):
        with open("rank/"+user_data[user_id]['emotion']+".txt",'wt') as f:
			#머리말
            f.write("최고점수 100\n")
	#
    linenum = 0
    rank_list = []

	#파일 읽기
    with open("rank/"+user_data[user_id]['emotion']+".txt",'rt') as fr:
        rank_list = fr.readlines()
	
	#
    check = 0
    for rank in rank_list:
        rscore = rank.strip().split(' ')[1]
		#중복입력 방지
        if score == float(rscore) and real_name == rank.strip().split(' ')[0]: 
            check = 1
            break
        elif score > float(rscore) :
			# 데이터 기록하고 기존 데이터 한줄씩 밀어버림
            rank_list.insert(linenum,'%s %.2f\n'%(real_name,score))
            check = 1
            break
        linenum +=1
		
    if check == 0 :
        rank_list.append('%s %.2f\n'%(real_name,score))

    with open("rank/"+user_data[user_id]['emotion']+".txt",'wt') as fw:
        fw.write(''.join(rank_list))

def rank_view(channel, user_id):
    emotion_list = ['화남','신나는','썩은미소','심각한','놀람']
    rank_list = []
    for emotion in emotion_list:
        if not os.path.isfile("rank/"+emotion+".txt" ):
            slack_web_client.chat_postEphemeral(
                channel=channel,
                user = user_id,
                text= "<" + emotion +">" + " 감정은 랭크가 없습니다."
            )
        else :
            with open("rank/"+emotion+".txt",'rt') as fr:
                rank_list = fr.readlines()
                buffer = "====================\n"
                buffer += "*"+emotion+" 감정 랭크*\n"
                for index,rank in enumerate(rank_list[1:11]):
                    ranker_info = rank.strip().split(' ')
                    buffer += "%-2d위  %s  %s점\n"%(index+1,ranker_info[0],ranker_info[1])
                slack_web_client.chat_postEphemeral(
                    channel=channel,
                    user = user_id,
                    text=buffer+"====================\n" 
                )



    


#---------------Reference Codes---------------------------
# 다른 사람이 메시지를 올렸을 때, 그 사람에게만 답장하기
# 참고: https://api.slack.com/methods/chat.postEphemeral
# slack_web_client.chat_postEphemeral(
#     channel=event_data["event"]["channel"],
#     user=event_data["event"]["user"],
#     text="조용히 하세요!"
# )

# 특정 채널에 파일 업로드하기
# 참고: https://api.slack.com/methods/files.upload
# file_name = "hello.txt"
# result = slack_web_client.files_upload(channels="#채널명", file=file_name)
# if not result["ok"]:
#     print(file_name + " 업로드에 실패했습니다.")
#     print("원인: " + result["error"])

# # 알람 메시지를 설정하기
# # 시간을 입력하는 형식은 아래 링크를 참고하세요:
# # https://get.slack.help/hc/en-us/articles/208423427#format-a-reminder
# # 참고: https://api.slack.com/methods/reminders.add
# slack_web_client.reminders_add(text="오늘 수업 끝!", time="18:00")

# # 다른 사람이 올린 메시지에 이모티콘 달기
# # 참고: https://api.slack.com/methods/reactions.add
# slack_web_client.reactions_add(
#   name="thumbsup",
#   channel=event_data["event"]["channel"],
#   timestamp=event_data["event"]["event_ts"]
# )

# # 유저를 이름으로 검색하여 ID 코드를 구하기
# # 참고: https://api.slack.com/methods/users.list
# response = slack_web_client.users_list()
# for user in response["members"]:
#     if user.name == "홍길동":
#         user_id = user.id
#         break


if __name__ == '__main__':
    letsHaveFun()
    app.run('0.0.0.0', port=5000)

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"
